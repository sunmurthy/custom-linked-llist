package com.custom.collection;

/**
 * 
 * @author Sunil-Murthy
 *
 */
public class Node {

	int data;
	Node next;

	Node(int d) {
		data = d;
		next = null;
	}

}