package com.custom.collection;

/**
 * 
 * @author Sunil-Murthy
 *
 */
public class TestLinkedList {

	public static void main(String[] args) {

		CustomLinkedList list = new CustomLinkedList();
		
		list.add(10);
		list.add(23);
		list.add(11);
		list.add(20);
		list.removeLast();
		list.deleteList();
		list.removeLast();
	}

}
