/**
 * 
 */
package com.custom.collection;

/**
 * @author Sunil-Murthy
 *
 */
public class CustomLinkedList {

	private Node head;

	/**
	 * delete all elements in the list
	 */
	void deleteList() {
		head = null;
		System.out.println("The list deleted successfully");
	}

	/**
	 * Inserts a new Node at front of the list.
	 * 
	 * @param new_data
	 */
	public void add(int new_data) {
		/*
		 * 1 & 2: Allocate the Node & Put in the data
		 */
		Node new_node = new Node(new_data);

		/* 3. Make next of new Node as head */
		new_node.next = head;

		/* 4. Move the head to point to new Node */
		head = new_node;

		System.out.println(new_data + " added successfully");
	}

	/**
	 * this method delete the last element in the list
	 * 
	 * @return int
	 */
	public int removeLast() {
		if (isEmpty()) {
			System.out.println(" The list is empty");
			return 0;
		} else {
			Node current = head;
			while (current.next.next != null) {
				current = current.next;
			}
			int x = current.next.data;
			current.next = null;
			System.out.println(x + " Removed successfully");
			return x;
		}

	}

	/**
	 * this method checks the list is empty or not
	 * 
	 * @return boolean
	 */
	public boolean isEmpty() {
		if (head == null)
			return true;
		else
			return false;
	}

}
